# Glossary Duel Browser Extension

The extension source code can be found in the `./src/` directory.
To build the application (tested on Windows 10):
1. Install node (v10.9.0) and yarn (v1.12.3).
2. Run `yarn`.
3. Run `npm run build`.
4. Go into the `./build/` directory and zip its contents.
