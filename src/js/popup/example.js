import overview from './overview.hbs';
import login from './login.hbs';
import * as service from "../lib/service";
import {get} from 'lodash';

export default function () {
  const container = document.querySelector("#app");

  service.liveAll(['profile', 'glossaries', 'glossariesTerms'], ({profile, glossaries, glossariesTerms}) => {
    const loggedIn = !!profile;
    if (loggedIn) {
      renderOverview(container, profile, glossaries, glossariesTerms);
    } else {
      renderLogin(container);
    }
  });
};

const renderLogin = (container) => {
  container.innerHTML = login();
  const usernameInput = container.querySelector("#username-input");
  const passwordInput = container.querySelector("#password-input");

  const errorOutput = container.querySelector("#error");
  const hideError = () => errorOutput.classList.add("hidden");
  usernameInput.addEventListener('input', hideError);
  passwordInput.addEventListener('input', hideError);

  container.querySelector("form").onsubmit = e => {
    e.preventDefault();

    errorOutput.classList.add("hidden");

    const username = usernameInput.value;
    const password = passwordInput.value;

    service.login(username, password)
      .catch(e => {
        console.error(e);
        errorOutput.innerText = get(e, 'response.data.message', `An error occured\n${e.toString()}`);
        errorOutput.classList.remove("hidden");
      });
  };
};

const renderOverview = (container, profile, glossaries, glossariesTerms) => {
  glossaries.forEach(glossary => glossary.loading = !glossariesTerms.hasOwnProperty(glossary.id));

  const context = {profile, glossaries};
  container.innerHTML = overview(context);

  const refreshButton = container.querySelector("#refresh");
  refreshButton.onclick = e => {
    e.preventDefault();

    service.update();
  };

  const logoutButton = container.querySelector("#logout");
  logoutButton.onclick = e => {
    e.preventDefault();

    service.logout();
  };
};
