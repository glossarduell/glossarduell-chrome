import {flatMap, mapValues, pick, without, zipObject} from "lodash";


const allKeys = ['authToken', 'profile', 'glossaries', 'glossariesTerms'];

const observers = [];

export const observeAll = (keys, callback) => {
  observers.push({keys, callback});
};

export const observe = (key, callback) => observeAll([key], (change) => callback(change[key]));

chrome.storage.onChanged.addListener(async (changes) => {
  const changed = Object.keys(changes);
  const matchingObservers = observers.filter(observer => observer.keys.some(observed => changed.includes(observed)));
  const watchedKeys = flatMap(matchingObservers, observer => observer.keys);
  const missingKeys = without(watchedKeys, ...changed);

  let items = mapValues(changes, change => change.newValue);
  if (missingKeys.length > 0) {
    const patch = await getAll(missingKeys);
    Object.assign(items, patch);
  }

  matchingObservers.forEach(observer => observer.callback(pick(items, observer.keys)))
});


export const getAll = (keys = allKeys) =>
  new Promise(resolve =>
    chrome.storage.local.get(keys, response => resolve(response))
  );

export const get = async (key) => (await getAll([key]))[key];


export const setAll = (items) =>
  new Promise(resolve =>
    chrome.storage.local.set(items, () => resolve())
  );

export const makeSetter = (...keys) => (...values) => setAll(zipObject(keys, values));

export const set = (key, value) => setAll({[key]: value});


export const liveAll = async (keys, callback) => {
  callback(await getAll(keys));
  observeAll(keys, callback);
};

export const live = async (key, callback) => {
  callback(await get(key));
  observe(key, callback);
};
