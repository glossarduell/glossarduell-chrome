import {get, set, observe, live, getAll, setAll, observeAll} from "./accessors";
export * from './accessors';

export const getAuthToken = () => get('authToken');
export const onAuthTokenChange = (observer) => observe('authToken', observer);
export const liveAuthToken = (observer) => live('authToken', observer);

export const getProfile = () => get('profile');
export const onProfileChange = (observer) => observe('profile', observer);
export const liveProfile = (observer) => live('profile', observer);

export const setUser = (authToken, profile) => setAll({authToken, profile});

export const getGlossaries = () => get('glossaries');
export const setGlossaries = (glossaries) => set('glossaries', glossaries);
export const onGlossariesChange = (observer) => observe('glossaries', observer);
export const liveGlossaries = (observer) => live('glossaries', observer);

export const getGlossariesTerms = () => get('glossariesTerms');
export const setGlossariesTerms = (glossariesTerms) => set('glossariesTerms', glossariesTerms);

export const getGlossaryTerms = async (glossaryId) => (await getGlossariesTerms())[glossaryId] || [];
export const setGlossaryTerms = async (glossaryId, terms) => setGlossariesTerms(Object.assign(await getGlossariesTerms(), {[glossaryId]: terms}));

export const getGlossariesData = () => getAll('glossaries', 'glossariesTerms');
export const setGlossariesData = (glossaries, glossariesTerms) => setAll({glossaries, glossariesTerms});
export const onGlossariesDataChange = (observer) => observeAll('glossaries', 'glossariesTerms');

export const reset = () => {
  setUser(null, null);
  setGlossariesData([], {});
};
