import axios from 'axios';
import applyConverters from 'axios-case-converter';
import * as store from './store';
import config from "../../config";

const API_URL = 'https://glossarduell.informatik.uni-hamburg.de/glossar/api/v2';

/** @type {AxiosInstance} */
const client = applyConverters(axios.create({
  baseURL: API_URL,
  headers: {
    'X-Source-Type': config.apiRequestSourceName,
  },
}));

store.liveAuthToken(token => setAuthToken(token));

export const setAuthToken = (authToken) => {
  client.defaults.headers.Secret = authToken;
};


export const login = (username, password) => {
  return client.post('/users/auth', {username, password});
};

export const getGlossaries = () => {
  return client.get('/glossaries');
};

export const getTerms = (glossaryId) => {
  return client.get(`/glossaries/${glossaryId}/terms`);
};

export const requestTerm = (glossaryId, term) => {
  return client.post(`/glossaries/${glossaryId}/terms`, {term});
};

export const trackViewDefinition = (definitionId) => {
  return client.post("/activities/view-definition", {
    definition: definitionId,
  });
};
