const BASE_URL = 'https://glossarduell.informatik.uni-hamburg.de';

export const toPath = (path) => `${BASE_URL}${path}`;
export const toTerm = (glossaryId, termId) => toPath(`/glossary/${glossaryId}/${termId}`);
