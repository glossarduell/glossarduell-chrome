import {reset, setUser} from "./store/index";
import * as api from "./api";
import {omit} from "lodash";
import {setGlossariesData, setGlossaryTerms} from "./store";

export * from './store';


export const login = async (username, password) => {
  const {data: response} = await api.login(username, password);
  const token = response.token;
  const profile = omit(response, 'token'); // TODO: ES6
  await setUser(token, profile);
  api.setAuthToken(token); // TODO: Observe auth token

  await update();
};

export const logout = async () => reset();

export const update = async () => {
  const {data: glossaries} = await api.getGlossaries();
  setGlossariesData(glossaries, {});

  for (let glossary of glossaries) {
    await updateGlossary(glossary.id);
  }
};

export const updateGlossary = async (glossaryId) => {
  const {data} = await api.getTerms(glossaryId);
  await setGlossaryTerms(glossaryId, data);
};

export const requestTerm = async (glossaryId, term) => {
  await api.requestTerm(glossaryId, term);
  await updateGlossary(glossaryId);
};
