export const definitionType = {
  addition: 'addition',
  suggestion: 'suggestion',
  merge: 'merge',
  review: 'review',
  main: 'main',
};
