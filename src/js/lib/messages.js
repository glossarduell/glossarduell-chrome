export const QUERY_TERM = "QUERY_TERM";
export const LIST_TERMS = "LIST_TERMS";
export const UPDATE_TERMS = "UPDATE_TERMS";
export const GET_ELEMENT_SELECTORS = "GET_ELEMENT_SELECTORS";
export const TRACK_VIEW_TERM = "TRACK_VIEW_TERM";

export const getElementSelectors = () => ({
  type: GET_ELEMENT_SELECTORS,
});

export const queryTerm = (term) => ({
  type: QUERY_TERM,
  term,
});

export const listTerms = () => ({
  type: LIST_TERMS,
});

export const updateTerms = () => ({
  type: UPDATE_TERMS,
});

export const trackViewTerm = (term) => ({
  type: TRACK_VIEW_TERM,
  term,
});
