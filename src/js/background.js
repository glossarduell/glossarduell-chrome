import '../img/icon-128.png';
import '../img/icon-48.png';
import '../img/icon-36.png';
import '../img/icon-32.png';
import '../img/icon-28.png';
import '../img/icon-24.png';
import '../img/icon-20.png';
import '../img/icon-16.png';
import * as messages from './lib/messages';
import {partition, mapValues, flatten} from 'lodash';
import {urlMatches} from "./background/inject";
import * as service from './lib/service';
import contextMenus from "./helpers/contextMenus";
import notify from "./helpers/notify";
import web from "./lib/web";
import {definitionType} from "./lib/constants";
import * as api from "./lib/api";
import config from "../config";


chrome.runtime.onInstalled.addListener(({reason}) => {
  if (reason === 'install') {
    service.reset();
  }
});

async function findMatchingGlossary(url) {
  const glossaries = await service.getGlossaries();
  return glossaries.find(glossary => glossary.pages.some(page => urlMatches(url, page.url)));
}

function findMatchingGlossaryPages(url, glossary) {
  return glossary.pages.filter(page => urlMatches(url, page.url));
}

chrome.tabs.onUpdated.addListener(async (tabId, changeInfo, tab) => {
  if (changeInfo.status === 'complete') {
    if (await findMatchingGlossary(tab.url)) {
      chrome.tabs.executeScript(tabId, {
        file: 'content.bundle.js',
      });
    }
  }
});


const makeHandlers = (handlerMap) => (message, sender, sendResponse) => {
  if (message && message.type in handlerMap) {
    findMatchingGlossary(sender.url).then(async glossary => {
      sendResponse(await handlerMap[message.type](glossary, message, sender));
    });
    return true;
  }
};

chrome.runtime.onMessage.addListener(makeHandlers({
  [messages.QUERY_TERM]: (glossary, message) => queryTerm(glossary, message.term),
  [messages.LIST_TERMS]: (glossary) => listTerms(glossary),
  [messages.GET_ELEMENT_SELECTORS]: (glossary, message, sender) => elementSelectors(glossary, sender.url),
  [messages.TRACK_VIEW_TERM]: (glossary, message) => trackViewTerm(glossary, message.term),
}));


async function queryTerm(glossary, termStr) {
  const term = await findTerm(glossary, termStr);
  if (term) {
    const showReview = term.workingCopy && term.workingCopy.type === definitionType.review;
    const displayedDefinition = showReview ? term.workingCopy : term.definition;
    const webUrl = web.url.toTerm(glossary.id, term.id);

    return {
      found: true,
      term: displayedDefinition.term,
      definition: displayedDefinition.definition,
      definitionId: displayedDefinition.id,
      review: showReview,
      webUrl,
    };
  } else {
    return {
      found: false,
    };
  }
}

async function listTerms(glossary) {
  const glossaryTerms = await service.getGlossaryTerms(glossary.id);
  const [requested, defined] = partition(glossaryTerms, term => term.definition.type === definitionType.addition);
  return mapValues({requested, defined}, terms => terms.map(term => term.definition.term));
}

async function elementSelectors(glossary, url) {
  return flatten(findMatchingGlossaryPages(url, glossary).map(page => page.elements));
}

async function trackViewTerm(glossary, termStr) {
  const term = await queryTerm(glossary, termStr);
  if (term.found) {
    await api.trackViewDefinition(term.definitionId);
  } else {
    console.warn(`Could not find highlighted term ${termStr}`);
  }
}


async function findTerm(glossary, termStr) {
  const glossaryTerms = await service.getGlossaryTerms(glossary.id);
  return glossaryTerms.find(term =>
    cleanString(term.definition.term) === cleanString(termStr)
  );
}

function cleanString(str) {
  return str.trim().toLowerCase();
}


let refreshIntervalHandle = null;
service.liveProfile((profile) => {
  const loggedIn = !!profile;
  if (loggedIn && !refreshIntervalHandle) {
    refreshIntervalHandle = setInterval(() => service.update(), config.glossariesRefreshInterval);
  } else if (!loggedIn && refreshIntervalHandle) {
    clearInterval(refreshIntervalHandle);
    refreshIntervalHandle = null;
  }
});


// Context menu

service.liveGlossaries(async (glossaries) => {
  await contextMenus.removeAll();

  if (glossaries && glossaries.length > 0) {
    await contextMenus.create({
      id: 'glossary',
      title: 'Request definition',
      contexts: ['selection'],
    });

    glossaries.forEach(glossary => {
      contextMenus.create({
        parentId: 'glossary',
        title: glossary.name,
        contexts: ['selection'],
        onclick: ({selectionText}) => handleRequestTerm(glossary, selectionText),
      });
    })
  }
});

async function handleRequestTerm(glossary, selectionText) {
  const term = selectionText.trim();

  try {
    await service.requestTerm(glossary.id, term);
    await notify("Definition requested", `Requested "${term}" for glossary "${glossary.name}"`);
    await updateAllTabs();
  } catch (e) {
    console.log(e);
    await notify("An error occured", `Term could not be requested\n${e.toString()}`);
  }
}

function updateAllTabs() {
  return new Promise(resolve => {
    chrome.tabs.query({}, async tabs => {
      await Promise.all(tabs.map(tab => new Promise(resolve =>
        chrome.tabs.sendMessage(tab.id, messages.updateTerms(), () => resolve())
      )));
      resolve();
    });
  });
}
