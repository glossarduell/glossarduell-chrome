import Popper from 'popper.js';
import isMouseDown from "../../helpers/isMouseDown";

export const defaultPopperOptions = {
  placement: 'bottom',
  removeOnDestroy: true,
  modifiers: {
    computeStyle: {
      gpuAcceleration: false
    },
    preventOverflow: {
      boundariesElement: "window",
    }
  },
};

const fadeAnimation = [
  { opacity: 0, transform: 'translateY(1.2rem)' },
  { opacity: 1, transform: 'translateY(0)' }
];

const fadeInTiming = {
  duration: 350,
  easing: 'ease',
};

const fadeOutTiming = {
  duration: 200,
  easing: 'ease',
};

export default class Tooltip {
  constructor(showDelay, hideDelay, reference, makeElement, popperOptions = defaultPopperOptions) {
    this.show = this.show.bind(this);
    this.hide = this.hide.bind(this);
    this.getElement = this.getElement.bind(this);
    this.queueState = this.queueState.bind(this);
    this.startShowTimeout = this.startShowTimeout.bind(this);
    this.clearShowTimeout = this.clearShowTimeout.bind(this);
    this.startHideTimeout = this.startHideTimeout.bind(this);
    this.clearHideTimeout = this.clearHideTimeout.bind(this);
    this.onShow = this.onShow.bind(this);

    this.showDelay = showDelay;
    this.hideDelay = hideDelay;
    this.popper = null;
    this.makeElement = makeElement;
    this.reference = reference;
    this.popperOptions = popperOptions;
    this.onShowListeners = [];

    this.reference.addEventListener("mouseover", () => this.queueState('visible'));
    this.reference.addEventListener("mouseout", () => this.queueState('hidden'));
  }

  get visible() {
    return !!this.popper;
  }

  async getElement() {
    const element = await this.makeElement();
    document.body.appendChild(element);
    return element;
  }

  async show() {
    if (!document.contains(this.reference)) {
      return;
    }

    this.clearShowTimeout();
    const element = await this.getElement();

    element.addEventListener("mouseover", () => this.queueState('visible'));
    element.addEventListener("mouseout", () => this.queueState('hidden'));

    this.popper = new Popper(this.reference, element, this.popperOptions);
    element.firstElementChild.animate(fadeAnimation, fadeInTiming);

    this.onShowListeners.forEach(listener => listener());
  }

  hide() {
    this.clearShowTimeout();

    if (this.popper) {
      const animation = this.popper.popper.firstElementChild.animate(fadeAnimation, fadeOutTiming);
      animation.onfinish = () => {
        this.popper.destroy();
        this.popper = null;
      };

      animation.reverse();
    }
  }

  queueState(state) {
    if (state === 'visible') {
      this.clearHideTimeout();
      if (!this.visible && !isMouseDown()) {
        this.startShowTimeout();
      }
    } else {
      this.clearShowTimeout();
      if (this.visible) {
        this.startHideTimeout();
      }
    }
  }

  startShowTimeout() {
    this.clearShowTimeout();
    this.showTimeoutHandle = window.setTimeout(() => this.show(), this.showDelay);
  }

  clearShowTimeout() {
    if (this.showTimeoutHandle) {
      window.clearTimeout(this.showTimeoutHandle);
      this.showTimeoutHandle = null;
    }
  }

  startHideTimeout() {
    this.clearHideTimeout();
    this.hideTimeoutHandle = window.setTimeout(() => this.hide(), this.hideDelay);
  }

  clearHideTimeout() {
    if (this.hideTimeoutHandle) {
      window.clearTimeout(this.hideTimeoutHandle);
      this.hideTimeoutHandle = null;
    }
  }

  onShow(listener) {
    this.onShowListeners.push(listener);
  }
}
