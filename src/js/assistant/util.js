export const createHtmlElement = (htmlString) => {
  const template = document.createElement("template");
  template.innerHTML = htmlString;
  return template.content.firstChild;
};

export const clearSelection = () => {
  const selection = document.getSelection();

  if (selection) {
    selection.removeAllRanges();
  }
};
