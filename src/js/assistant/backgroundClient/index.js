import * as messages from "../../lib/messages";

export default class BackgroundClient {
  constructor() {
    this.fetchTerms = this.fetchTerms.bind(this);
    this.onUpdate = this.onUpdate.bind(this);
    this.search = this.search.bind(this);

    this.updateListeners = [];

    chrome.runtime.onMessage.addListener((request) => {
      if (request.type === messages.UPDATE_TERMS) {
        this.fetchTerms();
      }
    });
  }

  fetchTerms() {
    chrome.runtime.sendMessage(messages.listTerms(), (terms) => {
      this.updateListeners.forEach(listener => listener(terms));
    });
  }

  onUpdate(listener) {
    this.updateListeners.push(listener);
  }

  search(term) {
    return new Promise(resolve => {
      chrome.runtime.sendMessage(messages.queryTerm(term), response => {
        resolve(response);
      });
    })
  }

  getElementSelectors() {
    return new Promise(resolve => {
      chrome.runtime.sendMessage(messages.getElementSelectors(), response => {
        resolve(response);
      })
    });
  }
}
