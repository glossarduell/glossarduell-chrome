import './styles.css';
import Popup from './popup';
import Tooltip from "./tooltip";
import Highlighter from "./highlighter";
import BackgroundClient from "./backgroundClient";
import {clearSelection} from "./util";
import * as messages from "../lib/messages";

export default class Assistant {
  constructor() {
    this.createTooltip = this.createTooltip.bind(this);
    this.makeTermPopup = this.makeTermPopup.bind(this);
    this.highlightTerms = this.highlightTerms.bind(this);
    this.updateTerms = this.updateTerms.bind(this);

    this.backgroundClient = new BackgroundClient();
    this.backgroundClient.getElementSelectors().then(elementSelectors => {
      this.highlighter = new Highlighter(elementSelectors);
      this.highlighter.onHighlight((marker) => this.createTooltip(marker.innerText, marker));

      this.backgroundClient.onUpdate((terms) => this.highlightTerms(terms));

      this.updateTerms();
    });
  }

  updateTerms() {
    this.backgroundClient.fetchTerms();
  }

  createTooltip(term, element) {
    const tooltip = new Tooltip(1000, 200, element, () => this.makeTermPopup(term));
    tooltip.onShow(() => chrome.runtime.sendMessage(messages.trackViewTerm(term)));
  }

  async makeTermPopup(term) {
    const response = await this.backgroundClient.search(term);
    if (response.found) {
      return Popup(response.term, response.definition, response.review, response.webUrl);
    } else {
      throw new Error(`Could not create popup for term ${term}`);
    }
  }

  highlightTerms({requested, defined}) {
    clearSelection();

    this.highlighter.clear();
    this.highlighter.highlight(requested, "term-requested");
    this.highlighter.highlight(defined, "term-defined");
  }

}