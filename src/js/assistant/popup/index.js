import {createHtmlElement} from "../util";
import PopoverTemplate from './template.html';
import './styles.css';

const emptyClass = 'definition-empty';
const badgeVisibleClass = 'definition-badge-visible';

export default function Popup(term, definition, review, webUrl) {
  const node = createHtmlElement(PopoverTemplate);
  const termNode = node.querySelector('[data-definition-term]');
  const bodyNode = node.querySelector('[data-definition-body]');
  const reviewBadgeNode = node.querySelector('[data-review-badge]');
  const openInWebLinkNode = node.querySelector('a[data-open-in-web]');

  termNode.innerText = term;

  if (definition) {
    bodyNode.innerText = definition;
    bodyNode.classList.remove(emptyClass);
  } else {
    bodyNode.innerText = "A definition for this term was requested";
    bodyNode.classList.add(emptyClass);
  }

  if (review) {
    reviewBadgeNode.classList.add(badgeVisibleClass);
  } else {
    reviewBadgeNode.classList.remove(badgeVisibleClass);
  }

  openInWebLinkNode.setAttribute('href', webUrl);

  return node;
};
