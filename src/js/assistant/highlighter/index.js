import Mark from 'mark.js';
import navigation from "./navigation";
import {debounce} from "lodash";


export default class Highlighter {
  constructor(elementSelectors) {
    this.highlight = this.highlight.bind(this);
    this._highlight = this._highlight.bind(this);
    this.update = this.update.bind(this);
    this.onHighlight = this.onHighlight.bind(this);
    this.clear = this.clear.bind(this);
    this.getElements = this.getElements.bind(this);

    this.highlights = [];
    this.highlightListeners = [];
    this.elementSelectors = elementSelectors;

    this.mark = new Mark(this.getElements());

    navigation.onPageUpdate(debounce(() => this.update(), 500, {maxWait: 3000}));
  }

  getElements() {
    return document.querySelectorAll(this.elementSelectors.join(', '));
  }

  highlight(terms, className) {
    this.highlights.push({terms, className});
    this._highlight(terms, className);
  }

  _highlight(terms, className) {
    this.mark.mark(terms, {
      exclude: ['input', 'div[contenteditable="true"]'],
      element: "term",
      className: className,
      separateWordSearch: false,
      each: marker => this.highlightListeners.forEach(listener => listener(marker)),
    });
  }

  update() {
    console.time("highlight");
    this.mark.unmark();

    // TODO: Reuse previous mark instance; broke after chrome store refactor
    this.mark = new Mark(this.getElements());
    this.highlights.forEach(({terms, className}) => this._highlight(terms, className));
    console.timeEnd("highlight");
  }

  onHighlight(listener) {
    this.highlightListeners.push(listener);
  }

  clear() {
    this.highlights = [];
    this.update();
  }
}
