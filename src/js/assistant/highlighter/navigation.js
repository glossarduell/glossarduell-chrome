import MutationSummary from 'mutation-summary';

class NavigationWatcher {
  constructor() {
    this.onPageUpdate = this.onPageUpdate.bind(this);
    this.processChanges = this.processChanges.bind(this);

    this.listeners = [];

    const observer = new MutationSummary({
      queries: [{element: '*'}],
      callback: (summaries) => this.processChanges(summaries),
    });
  }

  processChanges(summaries) {
    const summary = summaries[0];
    const {added, removed, reparented} = summary;
    const getOldParentNode = summary.getOldParentNode.bind(summary);
    const iterator = removedParentIterator([...removed, ...reparented], getOldParentNode);

    if ([added, removed, reparented].some(elements => !everyNodeFromExtension(elements, iterator))) {
      this.listeners.forEach(listener => listener());
    }
  }

  onPageUpdate(listener) {
    this.listeners.push(listener);
  }
}

const everyNodeFromExtension = (nodes, iterator) => nodes.every(node => hasSuccessor(node, isExtensionNode, iterator));

const parentIterator = (node) => node.parentNode;
const removedParentIterator = (removed, getOldParentNode) => (node) => removed.includes(node) ? getOldParentNode(node) : parentIterator(node);

const hasSuccessor = (initial, predicate, iterator) => {
  if (!initial) {
    return false;
  }

  let it = initial;
  do {
    if (predicate(it)) {
      return true;
    }

    it = iterator(it);
  } while (it);

  return false;
};

const isExtensionNode = (node) => {
  return node && (node.nodeName === 'TERM' || (node.getAttribute && node.getAttribute("data-glossary")));
};


if (!window.navigationWatcher) {
  window.navigationWatcher = new NavigationWatcher();
}

export default window.navigationWatcher;
