export default {
  removeAll: () =>
    new Promise(resolve => chrome.contextMenus.removeAll(() => resolve())),
  create: (properties) =>
    new Promise(resolve => chrome.contextMenus.create(properties, () => resolve())),
}
