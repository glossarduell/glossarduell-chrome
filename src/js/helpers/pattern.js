import Handlebars from "handlebars";
import placeholder from './templates/placeholder.hbs';

export default function (pattern) {
  const result = pattern
    .split('*')
    .map(part => Handlebars.Utils.escapeExpression(part))
    .map(part => part.replace(/\//g, '&#8203;/'))
    .join(placeholder());

  return new Handlebars.SafeString(result);
}
