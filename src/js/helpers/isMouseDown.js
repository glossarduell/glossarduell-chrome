if (document.mouseDownCounter === undefined) {
  document.mouseDownCounter = 0;

  document.body.addEventListener('mousedown', () => document.mouseDownCounter++);
  document.body.addEventListener('mouseup', () => document.mouseDownCounter--);
}

export default function isMouseDown() {
  return document.mouseDownCounter > 0;
}
