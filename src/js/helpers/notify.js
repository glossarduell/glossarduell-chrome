export default async function notify(title, body) {
  return new Promise(resolve => {
    chrome.notifications.create({
      type: 'basic',
      title: title,
      message: body,
      iconUrl: 'icon-128.png',
    }, () => resolve());
  });
}
