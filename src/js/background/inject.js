import matchPatternToRegExp from "../lib/matchPatternToRegExp";

export const urlMatches = (url, pattern) =>
  matchPatternToRegExp(pattern).test(url);
