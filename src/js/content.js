import Assistant from "./assistant";

if (!window.glossaryInjected) {
  window.glossaryInjected = true;

  new Assistant();
}
