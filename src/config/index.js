export default {
  apiRequestSourceName: 'desktop/extension',
  glossariesRefreshInterval: 30*60*1000, // 30min
};
